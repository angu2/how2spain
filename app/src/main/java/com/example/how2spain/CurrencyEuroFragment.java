package com.example.how2spain;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class CurrencyEuroFragment extends Fragment {

    private static ImageButton imageButtonCurrencyConvert;
    private TextView textViewResult;
    private TextView textViewLabelPLN;
    private EditText editTextNumberEuro;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_currency_euro, container, false);

        //return super.onCreateView(inflater, container, savedInstanceState);

        //przypisanie komponentow
        imageButtonCurrencyConvert = (ImageButton)view.findViewById(R.id.imageButtonCurrencyConvert);
        textViewResult = (TextView)view.findViewById(R.id.textViewResult);
        textViewLabelPLN = (TextView)view.findViewById(R.id.textViewLabelPLN);
        editTextNumberEuro = (EditText)view.findViewById(R.id.editTextNumberEuro);

        //listener for button
        OnClickCurrencyConvertListener();

        return view;
    }

    //listener for button convert
    public void OnClickCurrencyConvertListener() {
        imageButtonCurrencyConvert.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (editTextNumberEuro.getText().toString().isEmpty()){
                            editTextNumberEuro.setText("0");
                        }
                        //convertion to EUR
                        float euro = Float.parseFloat(editTextNumberEuro.getText().toString());
                        textViewResult.setText( String.valueOf(euro*4.3) );
                        textViewLabelPLN.setVisibility(View.VISIBLE);
                    }
                }
        );
    }
}

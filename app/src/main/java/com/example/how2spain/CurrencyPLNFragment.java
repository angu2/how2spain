package com.example.how2spain;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class CurrencyPLNFragment extends Fragment {

    private static ImageButton imageButtonCurrencyConvertTab2;
    private TextView textViewResultTab2;
    private TextView textViewLabelEuroTab2;
    private EditText editTextNumberPLNTab2;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_currency_pln, container, false);

        //przypisanie komponentow
        imageButtonCurrencyConvertTab2 = (ImageButton)view.findViewById(R.id.imageButtonCurrencyConvertTab2);
        textViewResultTab2 = (TextView)view.findViewById(R.id.textViewResultTab2);
        textViewLabelEuroTab2 = (TextView)view.findViewById(R.id.textViewLabelEuroTab2);
        editTextNumberPLNTab2 = (EditText)view.findViewById(R.id.editTextNumberPLNTab2);

        //listener for button
        OnClickCurrencyConvertListener();

        return view;

    }

    //listener for button convert
    public void OnClickCurrencyConvertListener() {
        imageButtonCurrencyConvertTab2.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (editTextNumberPLNTab2.getText().toString().isEmpty()){
                            editTextNumberPLNTab2.setText("0");
                        }

                        //convertion to PLN

                        textViewLabelEuroTab2.setVisibility(View.VISIBLE);

                        float pln = Float.parseFloat(editTextNumberPLNTab2.getText().toString());
                        //textViewResultTab2.setText( String.valueOf(pln*0.232558) );
                        textViewResultTab2.setText( String.format("%.3f",pln*0.232558) );
                    }
                }
        );
    }
}

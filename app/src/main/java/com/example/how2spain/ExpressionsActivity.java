package com.example.how2spain;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class ExpressionsActivity extends AppCompatActivity {

    private Button buttonExpressionsBasic;
    private Button buttonExpressionsNumbers;
    private Button buttonExpressionsDays;
    private Button buttonExpressionsMonths;
    private Button buttonExpressionsFamily;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expressions);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //listeners for button categories
        OnClickExpressionsBasicListener();
        OnClickExpressionsNumbersListener();
        OnClickExpressionsDaysListener();
        OnClickExpressionsMonthsListener();
        OnClickExpressionsFamilyListener();
    }

    //listener for Expressions Basic
    //launching ExpressionsBasicActivity
    public void OnClickExpressionsBasicListener() {
        buttonExpressionsBasic = (Button)findViewById(R.id.buttonExpressionsBasic);
        buttonExpressionsBasic.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(".ExpressionsBasicActivity");
                        startActivity(intent);
                    }
                }
        );
    }

    //listener for Expressions Numbers
    //launching ExpressionsNumbersActivity
    public void OnClickExpressionsNumbersListener() {
        buttonExpressionsNumbers = (Button)findViewById(R.id.buttonExpressionsNumbers);
        buttonExpressionsNumbers.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(".ExpressionsNumbersActivity");
                        startActivity(intent);
                    }
                }
        );
    }

    //listener for Expressions Days
    //launching ExpressionsDaysActivity
    public void OnClickExpressionsDaysListener() {
        buttonExpressionsDays = (Button)findViewById(R.id.buttonExpressionsDays);
        buttonExpressionsDays.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(".ExpressionsDaysActivity");
                        startActivity(intent);
                    }
                }
        );
    }

    //listener for Expressions Months
    //launching ExpressionsMonthsActivity
    public void OnClickExpressionsMonthsListener() {
        buttonExpressionsMonths = (Button)findViewById(R.id.buttonExpressionsMonths);
        buttonExpressionsMonths.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(".ExpressionsMonthsActivity");
                        startActivity(intent);
                    }
                }
        );
    }

    //listener for Expressions Family
    //launching ExpressionsFamilyActivity
    public void OnClickExpressionsFamilyListener() {
        buttonExpressionsFamily = (Button)findViewById(R.id.buttonExpressionsFamily);
        buttonExpressionsFamily.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(".ExpressionsFamilyActivity");
                        startActivity(intent);
                    }
                }
        );
    }
}

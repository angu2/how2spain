package com.example.how2spain;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static ImageView imageViewMainSmalltalk;
    private static ImageView imageViewMainExpressions;
    private static ImageView imageViewMainFood;
    private static ImageView imageViewMainTips;
    private static ImageView imageViewMainCurrency;
    private static ImageView imageViewMainSightseeing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //ikonka koperty
//        FloatingActionButton fab = findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        //onClickListeners
        OnClickMainSmalltalkListener();
        OnClickMainExpressionsListener();
        OnClickMainFoodListener();
        OnClickMainTipsListener();
        OnClickMainCurrencyListener();
        OnClickMainSightseeingListener();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_smalltalk) {
            LaunchSmallTalkActivity();

        } else if (id == R.id.nav_expressions) {
            LaunchExpressionsActivity();

        } else if (id == R.id.nav_sightseeing) {
            LaunchSightseeingActivity();

        } else if (id == R.id.nav_food) {
            LaunchFoodActivity();

        } else if (id == R.id.nav_currency) {
            LaunchCurrencyActivity();

        } else if (id == R.id.nav_tips){
            LaunchTipsActivity();

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //listener for imageViewMainSmalltalk
    //launching SmalltalkActivity
    public void OnClickMainSmalltalkListener() {
        imageViewMainSmalltalk = (ImageView)findViewById(R.id.imageViewMainSmalltalk);
        imageViewMainSmalltalk.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LaunchSmallTalkActivity();
                    }
                }
        );
    }

    private void LaunchSmallTalkActivity(){
        Intent intent = new Intent(".SmalltalkActivity");
        startActivity(intent);
    }

    //listener for imageViewMainSmalltalk
    //launching SmalltalkActivity
    public void OnClickMainExpressionsListener() {
        imageViewMainExpressions = (ImageView)findViewById(R.id.imageViewMainExpressions);
        imageViewMainExpressions.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LaunchExpressionsActivity();
                    }
                }
        );
    }

    private void LaunchExpressionsActivity(){
        Intent intent = new Intent(".ExpressionsActivity");
        startActivity(intent);
    }

    //listener for imageViewMainFood
    //launching FoodActivity
    public void OnClickMainFoodListener() {
        imageViewMainFood = (ImageView)findViewById(R.id.imageViewMainFood);
        imageViewMainFood.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LaunchFoodActivity();
                    }
                }
        );
    }

    private void LaunchFoodActivity(){
        Intent intent = new Intent(".FoodActivity");
        startActivity(intent);
    }

    //listener for imageViewMainTips
    //launching TipsActivity
    public void OnClickMainTipsListener() {
        imageViewMainTips = (ImageView)findViewById(R.id.imageViewMainTips);
        imageViewMainTips.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LaunchTipsActivity();
                    }
                }
        );
    }

    private void LaunchTipsActivity(){
        Intent intent = new Intent(".TipsActivity");
        startActivity(intent);
    }

    //listener for imageViewMainCurrency
    //launching CurrencyActivity
    public void OnClickMainCurrencyListener() {
        imageViewMainCurrency = (ImageView)findViewById(R.id.imageViewMainCurrency);
        imageViewMainCurrency.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LaunchCurrencyActivity();
                    }
                }
        );
    }

    private void LaunchCurrencyActivity(){
        Intent intent = new Intent(".CurrencyActivity");
        startActivity(intent);
    }

    //listener for imageViewMainSightseeing
    //launching SightseeingActivity
    public void OnClickMainSightseeingListener() {
        imageViewMainSightseeing = (ImageView)findViewById(R.id.imageViewMainSightseeing);
        imageViewMainSightseeing.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LaunchSightseeingActivity();
                    }
                }
        );
    }

    private void LaunchSightseeingActivity(){
        Intent intent = new Intent(".SightseeingActivity");
        startActivity(intent);
    }
}

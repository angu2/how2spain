package com.example.how2spain;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class SightseeingMalagaMapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sightseeing_malaga_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng aquamijas = new LatLng(36.538464, -4.633048);
        mMap.addMarker(new MarkerOptions().position(aquamijas).title("Park Wodny Aquamijas, Mijas"));

        LatLng crocodiles = new LatLng(36.624393, -4.508958);
        mMap.addMarker(new MarkerOptions().position(crocodiles).title("Crocodile Park, Torremolinos"));

        LatLng bioparc = new LatLng(36.536844, -4.627762);
        mMap.addMarker(new MarkerOptions().position(bioparc).title("BioParc Zoo, Fuengirola"));

        LatLng caminito = new LatLng(36.932114, -4.789961);
        mMap.addMarker(new MarkerOptions().position(caminito).title("El Caminito del Rey, Ardales"));

        LatLng jardin = new LatLng(36.617485, -4.560045);
        mMap.addMarker(new MarkerOptions().position(jardin).title("Jardin de las Aguilas, Benalmadena"));

        LatLng mariposario = new LatLng(36.589128, -4.581459);
        mMap.addMarker(new MarkerOptions().position(mariposario).title("Mariposario de Benalmadena"));

        LatLng teleferico = new LatLng(36.598887, -4.539820);
        mMap.addMarker(new MarkerOptions().position(teleferico).title("Kolejka Linowa, Teleférico Benalmádena"));

        LatLng sealife = new LatLng(36.596648, -4.515200);
        mMap.addMarker(new MarkerOptions().position(sealife).title("Akwarium Sea Life, Benalmadena"));

        LatLng nerja = new LatLng(36.749881, -3.868334);
        mMap.addMarker(new MarkerOptions().position(nerja).title("Kajakiem po morzu, Maro, Nerja, Malaga"));

        LatLng delfiny = new LatLng(36.547484, -4.624929);
        mMap.addMarker(new MarkerOptions().position(delfiny).title("Oglądanie delfinów, Fuengirola"));

        LatLng alcazaba = new LatLng(36.721171, -4.415862);
        mMap.addMarker(new MarkerOptions().position(alcazaba).title("Alcazaba, Malaga"));

        LatLng catedral = new LatLng(36.720225, -4.419286);
        mMap.addMarker(new MarkerOptions().position(catedral).title("Santa Iglesia Catedral Basílica de la Encarnación"));


        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(alcazaba,10f));

    }
}

package com.example.how2spain;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class SmalltalkActivity extends AppCompatActivity {

    private Button buttonSmalltalkAirport;
    private Button buttonSmalltalkRestaurant;
    private Button buttonSmalltalkShop;
    private Button buttonSmalltalkTransport;
    private Button buttonSmalltalkWay;
    private Button buttonSmalltalkHotel;
    private Button buttonSmalltalkDoctor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smalltalk);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //ikonka koperty
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        OnClickSmalltalkAirportListener();
        OnClickSmalltalkRestaurantListener();
        OnClickSmalltalkShopListener();
        OnClickSmalltalkTransportListener();
        OnClickSmalltalkWayListener();
        OnClickSmalltalkHotelListener();
        OnClickSmalltalkDoctorListener();
    }

    //listener for Smalltalk Airport
    //launching SmalltalkAirportActivity
    public void OnClickSmalltalkAirportListener() {
        buttonSmalltalkAirport = (Button)findViewById(R.id.buttonExpressionsBasic);
        buttonSmalltalkAirport.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(".SmalltalkAirportActivity");
                        startActivity(intent);
                    }
                }
        );
    }

    //listener for Smalltalk Restaurant
    //launching SmalltalkRestaurantActivity
    public void OnClickSmalltalkRestaurantListener() {
        buttonSmalltalkRestaurant = (Button)findViewById(R.id.buttonExpressionsNumbers);
        buttonSmalltalkRestaurant.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(".SmalltalkRestaurantActivity");
                        startActivity(intent);
                    }
                }
        );
    }

    //listener for Smalltalk Shop
    //launching SmalltalkShopActivity
    public void OnClickSmalltalkShopListener() {
        buttonSmalltalkShop = (Button)findViewById(R.id.buttonExpressionsDays);
        buttonSmalltalkShop.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(".SmalltalkShopActivity");
                        startActivity(intent);
                    }
                }
        );
    }

    //listener for Smalltalk Transport
    //launching SmalltalkTransportActivity
    public void OnClickSmalltalkTransportListener() {
        buttonSmalltalkTransport = (Button)findViewById(R.id.buttonExpressionsMonths);
        buttonSmalltalkTransport.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(".SmalltalkTransportActivity");
                        startActivity(intent);
                    }
                }
        );
    }

    //listener for Smalltalk Way
    //launching SmalltalkWayActivity
    public void OnClickSmalltalkWayListener() {
        buttonSmalltalkWay = (Button)findViewById(R.id.buttonExpressionsFamily);
        buttonSmalltalkWay.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(".SmalltalkWayActivity");
                        startActivity(intent);
                    }
                }
        );
    }

    //listener for Smalltalk Hotel
    //launching SmalltalkHotelActivity
    public void OnClickSmalltalkHotelListener() {
        buttonSmalltalkHotel = (Button)findViewById(R.id.buttonSmalltalkHotel);
        buttonSmalltalkHotel.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(".SmalltalkHotelActivity");
                        startActivity(intent);
                    }
                }
        );
    }

    //listener for Smalltalk Doctor
    //launching SmalltalkDoctorActivity
    public void OnClickSmalltalkDoctorListener() {
        buttonSmalltalkDoctor = (Button)findViewById(R.id.buttonSmalltalkDoctor);
        buttonSmalltalkDoctor.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(".SmalltalkDoctorActivity");
                        startActivity(intent);
                    }
                }
        );
    }
}
